// document.getElementById('video').play()
var menu = [
    {
        id: 0,
        name: 'Создать задание',
        link: true,
        border:true,
        open:false,
        margin:true,
        arrow:true,
        width:'',
        children: [
            {
            id: 0,
            name: 'Домашний мастер1',
            active:true,
            under:
                [
                    {
                        name: 'Электрик1',
                        url: ''
                    },
                    {
                        name: 'Электрик2',
                        url: ''
                    },
                ]
        },
            {
                id: 1,
                name: 'Домашний мастер2',
                under:
                    [
                        {
                            name: 'Электрик3',
                            url: ''
                        },
                        {
                            name: 'Электрик4',
                            url: ''
                        },
                    ]
            },
            {
                id: 2,
                name: 'Строительные материалы с доставкой',
                under:
                    [
                        {
                            name: 'Электрик5',
                            url: ''
                        },
                        {
                            name: 'Электрик6',
                            url: ''
                        },
                    ]
            }
            ]
    },
    {
        id: 1,
    name: 'Стать исполнителем',
    link: true,
    border:true,
    open:false,
    arrow:true,
    children: [
        {
        id: 0,
        name: 'Строительные материалы с доставкой',
        under:
            [
                {
                    name: 'Электрик',
                    url: ''
                },
                {
                    name: 'Электрик',
                    url: ''
                },
            ]
    },
        {
            id: 1,
            name: 'Домашний мастер',
            under:
                [
                    {
                        name: 'Электрик',
                        url: ''
                    },
                    {
                        name: 'Электрик',
                        url: ''
                    },
                ]
        }]
    },
    {
        id: 2,
        name: 'Все задания',
        link: false,
        url:''
    },
        {
            id: 3,
            name: 'Исполнители',
            link: false,
            url:''
        },
    {
        id: 4,
        beforeIco:'geo.svg',
        name: 'Грозный',
        link: false,
        url:''
    },
        {
            id: 5,
            beforeIco:'user.svg',
            name: 'Профиль',
            link: true,
            border: false,
            profile:true,
            children: [
                {
                    name:'Диалоги',
                    url:''
                },
                {
                    name:'Мои заказы',
                    url:''
                },
                {
                    name:'Мои услуги',
                    url:''
                },
                {
                    name:'Уведомления',
                    url:''
                },
                {
                    name:'Баланс',
                    url:''
                },
                {
                    name:'Профиль',
                    url:''
                },
                {
                    name:'Настройки уведомелний',
                    url:''
                },
                {
                    name:'Выход',
                    url:'',
                    exit:true
                },
            ]

        },



        ];
var nav = [
    {
        ico:'icoSpec.png',
        name: 'Спецтехника'
    },
    {
        ico:'icoTeh.png',
        name: 'Экспресс-доставка и перевозка грузов'
    },
    {
        ico:'icoPop.png',
        name: 'Популярные услуги'
    },
    {
        ico:'blizko.png',
        name: ''
    },
];
var app = new Vue({
    el: '#app',
    data: {
        menuOn:false,
        activeUnder:false,
        menu: menu,
        nav: nav,
        openProfile: false
    },
    methods:{
    openMenu: function (id) {

        if(!this.menu[id].profile) {
            console.log(id);
            // this.menu[id].open = this.menu[id].open && this.menu[id].link? false : true ;
            this.menu.map((value, index) => {
                if(value.profile){
                    this.openProfile=false;
                }
                return value.id == id ? (value.open ? value.open = false : value.open = true) : value.open = false;
            })
            this.menu[id].width = document.getElementById('link_' + id).getBoundingClientRect().right - document.getElementById('link_' + id).getBoundingClientRect().left;
        }
        else
        {
            console.log('profile',id)
            this.menu.map((value, index) => {
                return value.id == id ? (value.open ? value.open = false : value.open = true) : value.open = false;
            });
            this.openProfile=this.openProfile? false: true;
            this.menu[id].coord = document.getElementsByClassName('profile' )[0].getBoundingClientRect();
        }
    }


    }
});
